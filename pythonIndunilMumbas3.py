import boto3
s3 = boto3.client("s3")

def handler(event, context):
    try:
        data = s3.list_objects(
            Bucket="inducreatethiss3in-mumbai",
            MaxKeys=10
        )
    except BaseException as e:
        print(e)
        raise(e)
    
    return {"message": "Successfully 1 executed"}
